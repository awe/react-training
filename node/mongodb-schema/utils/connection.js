const mongoose = require("mongoose");
require("dotenv").config();
// const port = process.env.PORT || 27017;
// const url = `mongodb://localhost:${port}/trainingproject`;
// console.log("process.env.CONNECTION_STRING", process.env.CONNECTION_STRING);
const url = process.env.CONNECTION_STRING;

const connect = () =>
  mongoose.connect(url, { useNewUrlParser: true }).catch(error => {
    console.error("Error connecting to database", error);
  });

module.exports = connect;
