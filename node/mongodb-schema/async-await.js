const axios = require("axios");

const API_URL = "https://swapi.co/api/people/";

async function myFunction() {
  /*
  const data = await axios.get(API_URL);
  console.log("data.data", data.data);
  const third = data.data[2];
  console.log("third", third);
  */
  const response1 = axios.get("https://swapi.co/api/people/1");
  const response2 = axios.get("https://swapi.co/api/people/2");
  Promise.all([response1, response2]).then(function(values) {
    console.log(values);
  });
}

// get the 3rd item in the array
// then get their data

// myFunction();

function myFun() {
  return Promise.resolve(7);
}
// console.log(myFun());

// let allData = myFunction();

// let theThird = allData[2];

async function getTheThird(theThird) {
  const data = await axios.get(`${API_URL}2`);
  // console.log("data", data.data);
}

// getTheThird();

async function myFunc() {
  return axios.get(API_URL);
}

console.log(myFunc());
